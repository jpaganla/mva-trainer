### First-time setup of the code

After cloning the repository the folder
structure should ideally be the following:

```
MVA-Trainer/
    ├── config/
    ├── scripts/
    └── [other directories]
```

While [config](config) will host your config files, [scripts](scripts) hosts the main Python3 scripts as well as some helper modules.

If you are working on a local system the following command takes care of the entire setup:

```sh
source setup.sh
```
The setup script checks whether all required libraries are installed. In case you are missing any libraries please install them manually.
For this you can use in example `pip3`.

If you are working on lxplus the setup can be a little cumbersome. In that case I suggest you use a virtual environment and set up the packages manually.

#### Dockerising the code 

A `Dockerfile` is attached so you can create your own docker image of the code and run it:
```sh
docker build -t my_image --rm .
```
followed by:
```sh
docker run -it --name my_mva_trainer --rm my_image 
```
This will generate a docker container image called `my_image` and run a docker container named `my_mva_trainer`.
Upon calling the build command all dependencies are installed for you. You can then simply commence with sourcing the `setup.sh` script.
For information on how to save container data see [here](https://medium.com/swlh/dockerize-your-python-command-line-program-6a273f5c5544).

E.g. To get access to your `ntuple directory` you should do:
```sh
docker run -it --name my_mva_trainer --rm -v <absolute_path_to_Ntuple_directory>:/home/mva_trainer_user/Ntuples/ my_image
```
Where `<absolute_path_to_Ntuple_directory>` is the absolute path to your `ntuple directory`. This directory will then be mounted in the home directory of the docker container (here as *Ntuples*).

#### Usage on lxplus
From experience, setting up consistent python virtual environments on `lxplus` is not straightforward. At the same time, `docker` is not available there either. As an alternative, using `singularity` to create images can be used. After building the docker image, the following steps have to be taken for this:
```
# Setting Cache-directory outside /afs for disk quota reasons
export SINGULARITY_CACHEDIR=/tmp/$(whoami)/singularity
mkdir -r /tmp/$(whoami)/singularity

# Converting docker image into singularity image
singularity build my_singularity_image.sif my_image

# Starting shell session inside singularity (with mounts to /afs, /eos, /cvmfs)
singularity shell -B /afs -B /eos -B /cvmfs my_singularity_image.sif
```

In the ensuing singularity environment, the training workflow can then be carried out. At the end, singularity can be closed again with `exit`

## Typical Training Workflow
### Setting Options in Config File
The entire preprocessing, training and postprocessing process is controlled by **one** config file.
Please store your custom config files in [config](config).
Every time you perform a step of the training pipeline (see below) the used config file is automatically stored in your job directory. This way you can later always check the settings that were used for a particular step.

A config file consists of several main blocks:
* Exactly one `GENERAL` block
* At least one `SAMPLE` block
* Exactly one `DNNMODEL`, `BDTMODEL` or "WGANMODEL" block

#### GENERAL Options

| Option | Effect |
| ------ | ------ |
| Job | Name of the `Job`. An output directory is automatically created and all scripts will store their output in it.|
| MCWeight | c++ style weight string. Pass a string of (multiplied) `weights`: *weight_mc\*weight_pileup\*...*. |
| Selection | c++ style selection string. Pass a string of (boolean) criteria: *nJets>=2 && nEl==2* ... . |
| Treename | Name of ROOT `TTree` from which the data is taken in the conversion step. |
| Variables | List of variables that are read during the conversion step. Pass a comma-separated list: *pT_1jet,pT_2jet,pT_3jet,...* .|
| InputScaling | Determine procedure to scale inputs. Possible options are [minmax](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.MinMaxScaler.html) minmax_symmetric or [standard](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.StandardScaler.html) and `none`. If `minmax` is chosen inputs are scaled into a range between 0 and 1. If `standard` is chosen the input features are scaled by removing the mean and scaling to unit variance. If `none` is chosen no scaling is applied. Default is `minmax`. |
| WeightScaling | Can be set to `True` of `False`. If set to `True` weights are scaled such that the sum over all labels is normalised.|
| TreatNegWeights | Can be set to `True` of `False`. If set to `True` negative weights are set to 0.|
| Folds | Integer number which determines how many folds (k-folding) should be performed during training.|
| ATLASLabel | Text that is printed behind the ATLAS label.|
| CMLabel | CM label for the plots. *#sqrt{s}* is automatically added in front of this string.|
| CustomLabel | Custom text that is printed below the ATLAS label.|
| doCtrlPlots | Boolean deciding whether conversion control plots are produced Can be `True` or `False`.|
| Blinding | Option (float value) to blind data. Any datapoint beyond a given threshold (e.g. 0.5) will be blinded in the plot.|

#### Sample Options

| Option | Effect |
| ------ | ------ |
| Name | Name of the sample. |
| Type | Type of the sample. Can be `Signal`, `Background`, `Systematic`, or `Fake`|
| TrainLabel | Integer describing the label used during training.|
| NtupleFiles | Comma-separated list of ntuple files. Remember that the parent path is already given in the setup script.|
| Selection | Sample specific c++ style selection string. This selection is combined with the `GENERAL Selection`.|
| PenaltyFactor | Scalar value (float) that is multiplied to the training weight.|
| FillColor | Color value (integer) which is used to fill the corresponding Histogram in a stack plot. |
| Group | String to group together samples. These samples are now treated as "one". |

#### Model Options

| Option | Effect |
| ------ | ------ |
| Name | Custom name of the model. This name needs to be unique. I.e. if you plan to inject several models you need to make sure the names of the models are unambiguous.|
| Type | Type of the model. Currently supported: `Classification` (DNNMODEL), `Z-Reconstruction` (DNNMODEL) and `Classification-WGAN` (WGANMODEL).|
| Nodes | Comma-separated list of neurons for each layer.|
| Loss | Loss function which is used in the training of a `DNNMODEL` or `BDTModel`. Currently supported for `DNNMODEL`: `binary_crossentropy`, `categorical_crossentropy`, `mean_squared_error`, `mean_absolute_error`, `mean_absolute_percentage_error`, `mean_squared_logarithmic_error`, `cosine_similarity`, `huber_loss`, `log_cosh`, [`categorical_focal_loss`](https://arxiv.org/abs/1708.02002), [`binary_focal_loss`](https://arxiv.org/abs/1708.02002). For a `BDTMODEL` the following loss functions are supported: `deviance`, `exponential`.|
| Epochs | Number of training epochs for a `DNNMODEL` . Default is 100.|
| LearningRate | Initial learning rate for the training of a `DNNMODEL`. Default is 0.001.|
| BatchSize | Batch size used in training of a `DNNMODEL`. Default is 32.|
| ValidationSize | Relative size of the validation set used during training of a `DNNMODEL`. Default if 0.2.|
| nEstimators | Number of boosting stages to be performed during the training of a `BDTMODEL`.|
| MaxDepth | Maximum Depth of the individual estimators of a `BDTMODEL`.|
| Criterion | Function to measure the quality of a split in a `BDTMODEL`.| 
| Patience | Number of epochs with no improvement after which training will be stopped.|
| MinDelta | Minimum change in the monitored quantity to qualify as an improvement.|
| DropoutIndece | Layer indece at which [Dropout](https://keras.io/api/layers/regularization_layers/dropout/) layers are added. Only supported in `DNNMODEL`|
| DropoutProb | Probability of dropout. Default is 0.1.|
| BatchNormIndece | Layer indece at which [BatchNormalisation](https://keras.io/api/layers/normalization_layers/batch_normalization/) layers are added. Only supported in `DNNMODEL`|
| OutputSize | Number of neurons in the output layer of a `DNNMODEL`|
| ActivationFunctions | [Activation function](https://keras.io/api/layers/activations/) in the hidden layers. Pass a list of activation functions with a length that corresponds to the number of hidden layers defined in 'Nodes'. Used in `DNNMODEL`|
| OutputActivation | [Activation function](https://keras.io/api/layers/activations/) in the output layer of a `DNNMODEL`|
| Metrics | Comma-separated list of metrics to be evaluated during training. Supported for a `DNNMODEL`|
| ClassLabels | Custom classifier labels. Pass a comma-separated list of labels for multi-class classification.|
| ModelBinning | Custom binning using a fixed bin with. Pass a comma-separated list of values following the formar `nbins`, `x_low`, `x_high`. Default is 20,0,1. This binning will be used for all appropriate plots. In case of a multi-class classifier simply append different binnings (e.g. 20,0,1,10,0,1,5,0,1 for fixed binning with 20,10 and 5 bins respectively).|
| SmoothingAlpha | Float between 0 and 0.5 to be applied to smooth labels according using Y=Y(1-alpha)+alpha/K see [arXiv:1512.00567](https://arxiv.org/abs/1512.00567) for an introduction to label smoothing.|

### Converting root Files into hdf5 Files
The training scripts will only take hdf5 files as input. For this purpose you have to convert any root files you want to use using the [Converter script](scripts/Converter.py).

From the main directory you can do:

```sh
./scripts/Converter -c <config_path> --processes 8
```
Hereby `<config_path>` specifies the path to your config file and `processes` specifies the number of conversion processes running in parallel.
Upon running the script a new directory is created based on 'Job' option declared in the config file.
The directory structure looks like this:
```
<JobName>
    ├── Configs/
    ├── Data/
    ├── Model/
    └── Plots/
```
'Configs' will contain copies of all used config files. 'Data' will contain dataframes stored in '.h5' format. 'Model' will contain all model related files and 'Plots' will contain all control plots (and tables).

### Starting a Training
To start a training you can do:
```sh
./scripts/Trainer -c <config_path>
```
[The Trainer](scripts/Trainer.py) script processes the merged `.hdf5` file produced by the [converter script](scripts/Converter.py).
The trained model as well as the training history will be saved automatically.

### Evaluating a Training
To evaluate a training you can do:
```sh
./scripts/Evaluate -c <config_path> 
```
Control plots are created automatically.

### Inject back into Ntuples
To inject NN predictions back into Ntuples you can do:
```sh
./scripts/MVAInjector.py -i <input_path> -o <output_path> -c <config_path> -m <model_path> --processes <n_processes>
```
Hereby `<input_path>` describes the path to input ntuples. The `<output_path>` argument describes the path to an output directory in which the injected ntuples will be stored. The directory and the sub-directory structure will automatically be created for you. The config file is **again** passed via the `<config_path>` argument.
The trained model will **again** be taken from the `<model_path>`. You can pass multiple config files and multiple model paths to simultaneously inject multiple neural networks into Ntuples.
Using the *processes* argument you can specify a number of independent processes that are run simultaneously. Each process will inject the prediction into one root file and start a new process upon termination.
Keep in mind that this can be very memory-consuming because the entire root-file is loaded. Restrict this to a lower number (e.g. 2-4) in case you have memory limitations.
