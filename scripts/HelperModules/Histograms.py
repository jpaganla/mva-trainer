from array import array

from HelperModules.MessageHandler import InfoMessage


class Histograms:
    """This class defines binnings and labels for histograms.
    """
    def __init__(self):
        self.__eta_binning    = array('d',[-2.37,-1.7,-1.2,-1.0,-0.8,-0.6,-0.4,-0.2,0,0.2,0.4,0.6,0.8,1.0,1.2,1.7,2.37])
        self.__phi_binning    = (20,-3.141,3.141)
        self.__phimin_binning = (20, 0,3.141)
        self.__bTagWP_binning = (5,0.5,5.5)
        self.__dR_binning     = (20,0,6)

        self.__Label = {
            "ht"            : "HT [GeV]",
            "metsig"        : "E_{T}^{miss} sig.",
            "metsigHT"      : "E_{T}^{miss} sig. (HT)",
            "metsigET"      : "E_{T}^{miss} sig. (ET)",
            "eT_miss"       : "E_{T}^{miss} [GeV]",
            "phi_miss"      : "#phi^{miss}",
            "nJets"         : "Jet Multiplicity",
            "nBJets60"      : "b-Jet Multiplicity (60%)",
            "nBJets70"      : "b-Jet Multiplicity (70%)",
            "nBJets77"      : "b-Jet Multiplicity (77%)",
            "nBJets85"      : "b-Jet Multiplicity (85%)",
            "pT_1jet"       : "Jet p_{T,1} [GeV]",
            "pT_2jet"       : "Jet p_{T,2} [GeV]",
            "pT_3jet"       : "Jet p_{T,3} [GeV]",
            "pT_4jet"       : "Jet p_{T,4} [GeV]",
            "DL1r_1jet"     : "Jet DL1r_{1}",
            "DL1r_2jet"     : "Jet DL1r_{2}",
            "DL1r_3jet"     : "Jet DL1r_{3}",
            "DL1r_4jet"     : "Jet DL1r_{4}",
            "eta_1jet"      : "Jet #eta_{1}",
            "eta_2jet"      : "Jet #eta_{2}",
            "eta_3jet"      : "Jet #eta_{3}",
            "eta_4jet"      : "Jet #eta_{4}",
            "phi_1jet"      : "Jet #phi_{1}",
            "phi_2jet"      : "Jet #phi_{2}",
            "phi_3jet"      : "Jet #phi_{3}",
            "phi_4jet"      : "Jet #phi_{4}",
            "bTagWP_1jet"   : "b-tag WP_{1}",
            "bTagWP_2jet"   : "b-tag WP_{2}",
            "bTagWP_3jet"   : "b-tag WP_{3}",
            "bTagWP_4jet"   : "b-tag WP_{4}",
            "pT_1bjet"      : "Leading b-Jet p_{T} [GeV]",
            "pT_2bjet"      : "Sub-Leading b-Jet p_{T} [GeV]",
            "e_1bjet"       : "Leading b-Jet E [GeV]",
            "e_2bjet"       : "Sub-Leading b-Jet E [GeV]",
            "eta_1bjet"     : "b-Jet #eta_{T,1}",
            "eta_2bjet"     : "b-Jet #eta_{T,2}",
            "phi_1bjet"     : "b-Jet #phi_{T,1}",
            "phi_2bjet"     : "b-Jet #phi_{T,2}",
            "dphi_1bjet"    : "#Delta#phi b_{1}",
            "dphi_2bjet"    : "#Delta#phi b_{2}",
            "dRb1b2"        : "#DeltaR(b_{1},b_{2})",
            "dphimin2"      : "#Delta#phi(Jet_{2},E_{T}^{miss})_{min}",
            "dphimin3"      : "#Delta#phi(Jet_{3},E_{T}^{miss})_{min}",
            "dphimin4"      : "#Delta#phi(Jet_{4},E_{T}^{miss})_{min}",
            "pT_1lep"       : "Lepton p_{T,1} [GeV]",
            "pT_2lep"       : "Lepton p_{T,2} [GeV]",
            "pT_3lep"       : "Lepton p_{T,3} [GeV]",
            "pT_4lep"       : "Lepton p_{T,4} [GeV]",
            "eta_1lep"      : "Lepton #eta_{1}",
            "eta_2lep"      : "Lepton #eta_{2}",
            "eta_3lep"      : "Lepton #eta_{3}",
            "eta_4lep"      : "Lepton #eta_{4}",
            "phi_1lep"      : "Lepton #phi_{1}",
            "phi_2lep"      : "Lepton #phi_{2}",
            "phi_3lep"      : "Lepton #phi_{3}",
            "phi_4lep"      : "Lepton #phi_{4}",
            "dphi_1lep"     : "#Delta#phi(l_{1},E_{T}^{miss})_{min}",
            "dphi_2lep"     : "#Delta#phi(l_{2},E_{T}^{miss})_{min}",
            "dphi_3lep"     : "#Delta#phi(l_{3},E_{T}^{miss})_{min}",
            "dphi_4lep"     : "#Delta#phi(l_{4},E_{T}^{miss})_{min}",
            "mT_1lep"       : "Lepton m_{T,1} [GeV]",
            "mT_2lep"       : "Lepton m_{T,1} [GeV]",
            "mT_3lep"       : "Lepton m_{T,1} [GeV]",
            "mT_4lep"       : "Lepton m_{T,1} [GeV]",
            "dR_1lep_1bjet" : "#DeltaR (l_{1},b_{1})",
            "dR_2lep_1bjet" : "#DeltaR (l_{2},b_{1})",
            "dR_3lep_1bjet" : "#DeltaR (l_{3},b_{1})",
            "dR_4lep_1bjet" : "#DeltaR (l_{4},b_{1})",
            "dR_1lep_2bjet" : "#DeltaR (l_{1},b_{2})",
            "dR_2lep_2bjet" : "#DeltaR (l_{2},b_{2})",
            "dR_3lep_2bjet" : "#DeltaR (l_{3},b_{2})",
            "dR_4lep_2bjet" : "#DeltaR (l_{4},b_{2})",
            "mT_W"          : "m_{T}^{W} [GeV]",
            "min_nll"       : "min m(l,l) [GeV]",
            "mz1"           : "m_{Z} [GeV]",
            "ptz1"          : "p_{T,Z} [GeV]",
            "etaz1"         : "#eta_{Z}",
            "phiz1"         : "#phi_{Z}",
            "dphillz1"      : "#Delta#phi(l,l)^{Z}",
            "detallz1"      : "#Delta#eta(l,l)^{Z}",
            "dphiz1lep"     : "#Delta#phi(Z,l^{non-Z})",
            "pT_lepnonZ"    : "Lepton p_{T}^{non-Z}",
            "mz2"           : "m_{ll}^{non-Z} [GeV]",
            "ptz2"          : "p_{T,ll}^{non-Z} [GeV]",
            "etaz2"         : "#eta_{ll}^{non-Z}",
            "phiz2"         : "#phi_{ll}^{non-Z}",
            "dphillz2"      : "#Delta#phi(l,l)^{non-Z}",
            "detallz2"      : "#Delta#eta(l,l)^{non-Z}",
            "pT_z1_1lep"    : "Lepton p_{T,1}^Z [GeV]",
            "pT_z1_2lep"    : "Lepton p_{T,2}^Z [GeV]",
            "pT_z2_1lep"    : "Lepton p_{T,1}^{non-Z} [GeV]",
            "pT_z2_2lep"    : "Lepton p_{T,2}^{non-Z} [GeV]",
            "eta_z1_1lep"   : "Lepton #eta_{1}^{Z}",
            "eta_z1_2lep"   : "Lepton #eta_{2}^{Z}",
            "eta_z2_1lep"   : "Lepton #eta_{1}^{non-Z}",
            "eta_z2_2lep"   : "Lepton #eta_{2}^{non-Z}",
            "nJets40"       : "nJets_{40}",
            "Nmbjj_top"     : "Top Triplets",
            "LepTopMass"    : "m^{Top, lep.} [GeV]",
            "MMindRjj"      : "m_{min. #DeltaR}^{jj} [GeV]",
            "CosThetaBB"    : "|cos(#Theta)_{b,b}|",
            "DeltaRBbLl"    : "#DeltaR(b_{1}+b_{2},l_{Z,1}+l_{Z,2})",
            "CentrJet"      : "Centrality",
            "MaxMMindRlepb" : "m_{l,b}^{min. #DeltaR} [GeV]",
            "mbjj_top"      : "m_{bjj} [GeV]",
            "invMassTTZ"    : "m_{ttZ} [GeV]",
            "WZInvariantMass"          : "m_{WZ} [GeV]",
            "WZTransverseMomentum"     : "p_{T}^{WZ} [GeV]",
            "NJetPairsLowerMjjThanMbb" : "n_{m_{jj}}<n_{m_{bb}}",
            "deltaRMinus6jets"         : "#DeltaR(jets_{l},Z)-#DeltaR(b_{1},b_{2})",
            "nBJets85"      : "nBJets(WP 85)",
            "nBJets77"      : "nBJets(WP 77)",
            "nBJets70"      : "nBJets(WP 70)",
            "type_z1_1lep"  : "type(l_{1}^{Z})",
            "type_z1_2lep"  : "type(l_{2}^{Z})",
            "type_z2_1lep"  : "type(l_{1}^{non-Z})",
            "type_z2_2lep"  : "type(l_{2}^{non-Z})",
            "type_1lep"     : "type(l_{1})",
            "type_2lep"     : "type(l_{2})",
            "type_3lep"     : "type(l_{3})",
            "type_4lep"     : "type(l_{4})",
            "pseudo_dphi_ztoplep" : "#Delta#phi(Z,t_{lep.})",
            "pseudo_theta_starZ"  : "#theta^*(Z)",
            "pseudo_y_tophad"     : "Rapidity t_{had.}",
            "pseudo_y_toplep"     : "Rapidity t_{lep.}",
            "pseudo_m_tophad"     : "m(t)_{had.} [GeV]",
            "pseudo_m_toplep"     : "m(t)_{lep.} [GeV]",
            "pseudo_phi_tophad"   : "#phi(t)_{had.}",
            "pseudo_phi_toplep"   : "#phi(t)_{lep.}",
            "pseudo_eta_tophad"   : "#eta(t)_{had.}",
            "pseudo_eta_toplep"   : "#eta(t)_{lep.}",
            "pseudo_pT_tophad"    : "p_{T}(t)_{had.} [GeV]",
            "pseudo_pT_toplep"    : "p_{T}(t)_{lep.} [GeV]",
            "pseudo_m_ttbar"      : "m(t#bar{t}) [GeV]",
            "pseudo_m_ttZ"        : "m(t#bar{t}Z) [GeV]",
            "pseudo_mblv_weight"  : "mbl#nu weight",
            "m_toplep_discriminator"    : "Top_{lep} Discriminant",
            "mblv_weight_discriminator" : "mbl#nu Discriminant",
            "m_tophad"   : "m(t)_{had.} [GeV]",
            "m_toplep"   : "m(t)_{lep.} [GeV]",
            "phi_tophad" : "#phi(t)_{had.}",
            "phi_toplep" : "#phi(t)_{lep.}",
            "eta_tophad" : "#eta(t)_{had.}",
            "eta_toplep" : "#eta(t)_{lep.}",
            "pT_tophad"  : "p_{T}(t)_{had.} [GeV]",
            "pT_toplep"  : "p_{T}(t)_{lep.} [GeV]",
            "m_ttbar"    : "m(t#bar{t}) [GeV]",
            "m_ttZ"      : "m(t#bar{t}Z) [GeV]"
        }

        self.__Binning = {
            "ht"            : (20,0,1000),
            "eT_miss"       : (20,0,200),
            "metsig"        : (20,0,20),
            "metsigHT"      : (20,0,20),
            "metsigET"      : (20,0,20),
            "phi_miss"      : self.__phi_binning,
            "nJets"         : (8,-0.5,7.5),
            "nBJets60"      : (4,-0.5,3.5),
            "nBJets70"      : (4,-0.5,3.5),
            "nBJets77"      : (4,-0.5,3.5),
            "nBJets85"      : (4,-0.5,3.5),
            "pT_1jet"       : (20,20,240),
            "pT_2jet"       : (20,20,140),
            "pT_3jet"       : (20,20,60),
            "pT_4jet"       : (20,20,60),
            "DL1r_1jet"     : (6,-0.5,5.5),
            "DL1r_2jet"     : (6,-0.5,5.5),
            "DL1r_3jet"     : (6,-0.5,5.5),
            "DL1r_4jet"     : (6,-0.5,5.5),
            "eta_1jet"      : (len(self.__eta_binning)-1, self.__eta_binning),
            "eta_2jet"      : (len(self.__eta_binning)-1, self.__eta_binning),
            "eta_3jet"      : (len(self.__eta_binning)-1, self.__eta_binning),
            "eta_4jet"      : (len(self.__eta_binning)-1, self.__eta_binning),
            "phi_1jet"      : self.__phi_binning,
            "phi_2jet"      : self.__phi_binning,
            "phi_3jet"      : self.__phi_binning,
            "phi_4jet"      : self.__phi_binning,
            "bTagWP_1jet"   : self.__bTagWP_binning,
            "bTagWP_2jet"   : self.__bTagWP_binning,
            "bTagWP_3jet"   : self.__bTagWP_binning,
            "bTagWP_4jet"   : self.__bTagWP_binning,
            "pT_1bjet"      : (20,20,300),
            "pT_2bjet"      : (20,20,100),
            "e_1bjet"       : (20,20,300),
            "e_2bjet"       : (20,20,100),
            "eta_1bjet"     : (len(self.__eta_binning)-1, self.__eta_binning),
            "eta_2bjet"     : (len(self.__eta_binning)-1, self.__eta_binning),
            "phi_1bjet"     : self.__phi_binning,
            "phi_2bjet"     : self.__phi_binning,
            "dphi_1bjet"    : self.__phi_binning,
            "dphi_2bjet"    : self.__phi_binning,
            "dRb1b2"        : self.__phimin_binning,
            "dphimin2"      : self.__phimin_binning,
            "dphimin3"      : self.__phimin_binning,
            "dphimin4"      : self.__phimin_binning,
            "pT_1lep"       : (20,7,300),
            "pT_2lep"       : (20,7,200),
            "pT_3lep"       : (20,7,100),
            "pT_4lep"       : (20,7,50),
            "eta_1lep"      : (len(self.__eta_binning)-1, self.__eta_binning),
            "eta_2lep"      : (len(self.__eta_binning)-1, self.__eta_binning),
            "eta_3lep"      : (len(self.__eta_binning)-1, self.__eta_binning),
            "eta_4lep"      : (len(self.__eta_binning)-1, self.__eta_binning),
            "phi_1lep"      : self.__phi_binning,
            "phi_2lep"      : self.__phi_binning,
            "phi_3lep"      : self.__phi_binning,
            "phi_4lep"      : self.__phi_binning,
            "dphi_1lep"     : self.__phi_binning,
            "dphi_2lep"     : self.__phi_binning,
            "dphi_3lep"     : self.__phi_binning,
            "dphi_4lep"     : self.__phi_binning,
            "mT_1lep"       : (20,0,400),
            "mT_2lep"       : (20,0,300),
            "mT_3lep"       : (20,0,200),
            "mT_4lep"       : (20,0,100),
            "dR_1lep_1bjet" : self.__dR_binning,
            "dR_2lep_1bjet" : self.__dR_binning,
            "dR_3lep_1bjet" : self.__dR_binning,
            "dR_4lep_1bjet" : self.__dR_binning,
            "dR_1lep_2bjet" : self.__dR_binning,
            "dR_2lep_2bjet" : self.__dR_binning,
            "dR_3lep_2bjet" : self.__dR_binning,
            "dR_4lep_2bjet" : self.__dR_binning,
            "mT_W"          : (20,10,200),
            "min_nll"       : (20,0,200),
            "mz1"           : (20,80,100),
            "ptz1"          : (20,20,420),
            "etaz1"         : (len(self.__eta_binning)-1, self.__eta_binning),
            "phiz1"         : self.__phi_binning,
            "dphillz1"      : self.__phimin_binning,
            "detallz1"      : (20,0,3.5),
            "dphiz1lep"     : self.__phimin_binning,
            "pT_lepnonZ"    : (20,0,260),
            "mz2"           : (20,20,160),
            "ptz2"          : (20,20,420),
            "etaz2"         : (len(self.__eta_binning)-1, self.__eta_binning),
            "phiz2"         : self.__phi_binning,
            "dphillz2"      : self.__phimin_binning,
            "detallz2"      : (20,0,3.5),
            "pT_z1_1lep"    : (20,0,400),
            "pT_z1_2lep"    : (20,0,200),
            "pT_z2_1lep"    : (20,20,200),
            "pT_z2_2lep"    : (20,20,100),
            "eta_z1_1lep"   : (len(self.__eta_binning)-1, self.__eta_binning),
            "eta_z1_2lep"   : (len(self.__eta_binning)-1, self.__eta_binning),
            "eta_z2_1lep"   : (len(self.__eta_binning)-1, self.__eta_binning),
            "eta_z2_2lep"   : (len(self.__eta_binning)-1, self.__eta_binning),
            "nJets40"       : (7,-0.5,6.5),
            "Nmbjj_top"     : (3,-0.5,2.5),
            "LepTopMass"    : (20,0,400),
            "MMindRjj"      : (20,0,600),
            "CosThetaBB"    : (20,0,0.6),
            "DeltaRBbLl"    : (20,0,5),
            "CentrJet"      : (20,0,1),
            "MaxMMindRlepb" : (20,0,500),
            "mbjj_top"      : (20,40,360),
            "invMassTTZ"    : (20,0,1200),
            "WZInvariantMass"          : (20,160,800),
            "WZTransverseMomentum"     : (20,0,300),
            "NJetPairsLowerMjjThanMbb" : (20,0,1),
            "deltaRMinus6jets"         : (20,-4,4),
            "nBJets85"      : (6,-0.5,5.5),
            "nBJets77"      : (6,-0.5,5.5),
            "nBJets70"      : (6,-0.5,5.5),
            "type_z1_1lep"  : (4,10.5,14.5),
            "type_z1_2lep"  : (4,10.5,14.5),
            "type_z2_1lep"  : (4,10.5,14.5),
            "type_z2_2lep"  : (4,10.5,14.5),
            "type_1lep"     : (4,10.5,14.5),
            "type_2lep"     : (4,10.5,14.5),
            "type_3lep"     : (4,10.5,14.5),
            "type_4lep"     : (4,10.5,14.5),
            "pseudo_dphi_ztoplep" : (20,0,3.2),
            "pseudo_theta_starZ"  : (20,0,3.2),
            "pseudo_y_toplep"     : (20,-4,4),
            "pseudo_y_tophad"     : (20,-4,4),
            "pseudo_m_tophad"     : (20,60,300),
            "pseudo_m_toplep"     : (20,60,300),
            "pseudo_phi_tophad"   : self.__phi_binning,
            "pseudo_phi_toplep"   : self.__phi_binning,
            "pseudo_eta_tophad"   : (len(self.__eta_binning)-1, self.__eta_binning),
            "pseudo_eta_toplep"   : (len(self.__eta_binning)-1, self.__eta_binning),
            "pseudo_pT_tophad"    : (20,0,400),
            "pseudo_pT_toplep"    : (20,0,400),
            "pseudo_m_ttbar"      : (20,200,1400),
            "pseudo_m_ttZ"        : (20,200,1600),
            "pseudo_mblv_weight"  : (20,0,1),
            "m_toplep_discriminator"    : (20,0,400),
            "mblv_weight_discriminator" : (20,0,1),
            "m_tophad"   : (20,60,300),
            "m_toplep"   : (20,60,300),
            "phi_tophad" : self.__phi_binning,
            "phi_toplep" : self.__phi_binning,
            "eta_tophad" : (len(self.__eta_binning)-1, self.__eta_binning),
            "eta_toplep" : (len(self.__eta_binning)-1, self.__eta_binning),
            "pT_tophad"  : (20,0,400),
            "pT_toplep"  : (20,0,400),
            "m_ttbar"    : (20,200,1400),
            "m_ttZ"      : (20,200,1600)
        }

        self.__pathCharReplDict = {
            "/"        : "_DIV_",
            "*"        : "_TIMES_"
        }

    def get_Binning(self,var):
        return self.__Binning[var]

    def get_Label(self,var):
        return self.__Label[var]

    def is_valid(self,var):
        return var in self.__Label and var in self.__Binning

    def filter_VarPathName(self, var):
        """Filters out illegal chars from variable names for filename usage

        For variable Ctrl-plots, some characters are illegal in the filename.
        This function replaces the illegal chars in `__pathCharReplDict` by
        the strings specified there and removes all other chars not allowed
        in `__char_isAllowed`.

        Parameters
        ----------
        var : string
            Variable name to be filtered by the rules outlined above.
        
        Returns
        -------
        string
            Filtered variable name suitable for filenames.
        """
        
        varpath = var
        # Replacing chars by their replacement for readability
        for char, repl in self.__pathCharReplDict.items():
            varpath = varpath.replace(char, repl)
        
        # Removing other special characters
        varpath = ''.join([c for c in varpath if self.__char_isAllowed(c)])
        
        # TODO: Enable InfoMessage again once verbosity option added
        # if not var == varpath:
        #     InfoMessage("Filename '{:s}' used for variable '{:s}'".format(
        #             varpath, var))

        return varpath

    def __char_isAllowed(self, c):
        '''Returns True if char c is allowed, False otherwise'''
        return (c.isalnum() or c in " _-.,[]()")

