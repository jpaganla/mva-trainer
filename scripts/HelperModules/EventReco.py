from ROOT import TLorentzVector
from HelperModules.MessageHandler import ErrorMessage
import numpy as np

# some variables which are necessary for truth-reco matching
RecoVariables = ["pT_1jet","pT_2jet","pT_3jet","pT_4jet",
                 "e_1jet","e_2jet","e_3jet","e_4jet",
                 "eta_1jet","eta_2jet","eta_3jet","eta_4jet",
                 "phi_1jet","phi_2jet","phi_3jet","phi_4jet",
                 "pT_1lep","pT_2lep","pT_3lep","pT_4lep",
                 "e_1lep","e_2lep","e_3lep","e_4lep",
                 "eta_1lep","eta_2lep","eta_3lep","eta_4lep",
                 "phi_1lep","phi_2lep","phi_3lep","phi_4lep",
                 "charge_1lep", "charge_2lep", "charge_3lep", "charge_4lep",
                 "type_1lep", "type_2lep", "type_3lep", "type_4lep",
                 "eventNumber"]
TruthVariables = ["MC_b_from_t_pt","MC_b_from_tbar_pt",
                  "MC_b_from_t_m","MC_b_from_tbar_m",
                  "MC_b_from_t_eta","MC_b_from_tbar_eta",
                  "MC_b_from_t_phi","MC_b_from_tbar_phi",
                  "MC_Wdecay1_from_t_pdgId","MC_Wdecay1_from_tbar_pdgId",
                  "MC_Wdecay2_from_t_pdgId","MC_Wdecay2_from_tbar_pdgId",
                  "MC_Wdecay1_from_t_pt","MC_Wdecay1_from_tbar_pt",
                  "MC_Wdecay2_from_t_pt","MC_Wdecay2_from_tbar_pt",
                  "MC_Wdecay1_from_t_eta","MC_Wdecay1_from_tbar_eta",
                  "MC_Wdecay2_from_t_eta","MC_Wdecay2_from_tbar_eta",
                  "MC_Wdecay1_from_t_phi","MC_Wdecay1_from_tbar_phi",
                  "MC_Wdecay2_from_t_phi","MC_Wdecay2_from_tbar_phi",
                  "MC_Wdecay1_from_t_m","MC_Wdecay1_from_tbar_m",
                  "MC_Wdecay2_from_t_m","MC_Wdecay2_from_tbar_m",
                  "MC_Zdecay1_pt","MC_Zdecay2_pt",
                  "MC_Zdecay1_eta","MC_Zdecay2_eta",
                  "MC_Zdecay1_phi","MC_Zdecay2_phi",
                  "MC_Zdecay1_m","MC_Zdecay2_m",
                  "MC_Zdecay1_pdgId","MC_Zdecay2_pdgId",
                  "eventNumber"]

def MatchJetsRecoTruth(truth_indece, truth, reco_indece, reco , to_be_matched, DR_cut = 0.3):
    """
    This is a recursive algorithm that performs reco-truth matching for jets based on a deltaR cut of <0.3.

    Keyword Aaguments:
    truth_indece  --- list of integers representing truth indece
    truth         --- list of TLorentzVectors representing truth objects
    reco_indece   --- list of integers representing truth indece
    reco          --- list of TLorentzVectors representing truth objects
    to_be_matched --- list of integers representing reco indece still to be matched
    """
    if len(reco)== 0:
        return
    DR = 99999
    pop_truth = -1
    pop_reco = -1
    minimum = 99999
    for i in range(len(truth)):
        for j in range(len(reco)):
            DR = truth[i].DeltaR(reco[j])
            if(DR<minimum and DR < DR_cut):
                minimum = DR;
                pop_truth = i;
                pop_reco = j;
    if (pop_truth == -1 or pop_reco == -1):
        return
    else:
        reco_indece[to_be_matched[pop_reco]] = truth_indece[pop_truth]
        del to_be_matched[pop_reco]
        del truth[pop_truth]
        del reco[pop_reco]
        del truth_indece[pop_truth]
        return MatchJetsRecoTruth(truth_indece, truth, reco_indece, reco, to_be_matched)

def MatchLeptonsRecoTruth(truth_indece, truth, truth_charge_type, reco_indece, reco, reco_charge_type, to_be_matched, DR_cut = 0.1):
    """
    This is a recursive algorithm that performs reco-truth matching for leptons based on a deltaR cut of 0.1.

    Keyword Aaguments:
    truth_indece  --- list of integers representing truth indece
    truth         --- list of TLorentzVectors representing truth objects
    truth_charge_type   --- list of tuples (charge, pdgid) for truth objects
    reco_indece   --- list of integers representing truth indece
    reco          --- list of TLorentzVectors representing truth objects
    reco_charge_type   --- list of tuples (charge, pdgid) for reco objects
    to_be_matched --- list of integers representing reco indece still to be matched
    """
    if len(reco)== 0:
        return
    DR = 99999
    pop_truth = -1
    pop_reco = -1
    minimum = 99999
    for i in range(len(truth)):
        for j in range(len(reco)):
            DR = truth[i].DeltaR(reco[j])
            #ask for lepton pair with smallest DR, same charge and same flavour
            if(DR<minimum and DR < DR_cut and truth_charge_type[i][0] == reco_charge_type[j][0] and truth_charge_type[i][1] == reco_charge_type[j][1]): 
                minimum = DR;
                pop_truth = i;
                pop_reco = j;
    if (pop_truth == -1 or pop_reco == -1):
        return
    else:
        reco_indece[to_be_matched[pop_reco]] = truth_indece[pop_truth]
        del to_be_matched[pop_reco]
        del truth[pop_truth]
        del reco[pop_reco]
        del truth_indece[pop_truth]
        del truth_charge_type[pop_truth]
        del reco_charge_type[pop_reco]
        return MatchLeptonsRecoTruth(truth_indece, truth, truth_charge_type, reco_indece, reco, reco_charge_type, to_be_matched)

class RecoObject(object):
    """
    Class representing reconstructed objects such as charged leptons or jets. The purpose is to easily get their 4-vectors.

    Keyword arguments:
    pt  --- Transverse momentum
    eta --- Pseudorapidity
    phi --- Azimuthal angle
    e   --- Energy
    """
    def __init__(self, pt, phi, eta, e):
        self.__pt  = pt
        self.__phi = phi
        self.__eta = eta
        self.__e   = e
        self.__v   = TLorentzVector()
        self.__v.SetPtEtaPhiE(self.__pt, self.__eta, self.__phi, self.__e)

    def Vector(self):
        return self.__v

class RecoLepObject(RecoObject):
    """
    Class adding additional information if reconstructed object is a lepton. They are needed for the matching of leptons.
    
    Keyword arguments:
    charge --- charge of lepton
    lep_type --- PDGID of lepton
    """
    def __init__(self, pt, phi, eta, e, charge, lep_type):
        self.__charge = charge
        self.__type = lep_type
        super().__init__(pt, phi, eta, e)

    def ChargeType(self):
        return (self.__charge, self.__type)

class TruthObject(object):
    """
    Class representing truth objects such as leptons or jets. The purpose is to easily get their 4-vectors.

    Keyword arguments:
    pt  --- Transverse momentum in MeV
    eta --- Pseudorapidity
    phi --- Azimuthal angle
    m   --- Mass in MeV
    """
    def __init__(self, pt, phi, eta, m):
        self.__pt  = pt/1000 # Need to convert to GeV
        self.__phi = phi
        self.__eta = eta
        self.__m   = m/1000 # Need to convert to GeV
        self.__v   = TLorentzVector()
        self.__v.SetPtEtaPhiM(self.__pt, self.__eta, self.__phi, self.__m)

    def Vector(self):
        return self.__v

class TruthLepObject(TruthObject):
    """
    Class adding additional information if truth object is a lepton. They are needed for the matching of leptons.
    
    Keyword arguments:
    pdgId --- PDGID of lepton
    """
    def __init__(self,pt, phi, eta, m, pdgId):
        self.__charge = -np.sign(pdgId)
        self.__type = abs(pdgId)
        super().__init__(pt, phi, eta, m)
    
    def ChargeType(self):
        return (self.__charge, self.__type)

class Event(object):
    """
    Class for event reconstruction.

    Keyword arguments:
    VariablesDict --- Dictionary holding all variables necessary for event reconstruction and definition.
    """
    def __init__(self, VariablesDict, Channel):
        self.__VariablesDict = VariablesDict
        self.__Channel       = Channel
        self.__JetIndeceDict = {"b_from_t"     : 0,
                                "b_from_tbar"  : 1,
                                "q1_from_t"    : 2,
                                "q2_from_t"    : 3,
                                "q1_from_tbar" : 4,
                                "q1_from_tbar" : 5}
        self.__LeptonIndeceDict = {"l1_from_Z"   : 0,
                                   "l2_from_Z"   : 1,
                                   "l_from_t"    : 2,
                                   "l_from_tbar" : 3}

        self.__Z_matched            = False
        self.__Z_half_matched       = False
        self.__l_from_t_matched     = False
        self.__l_from_tbar_matched  = False
        self.__b_from_t_matched     = False
        self.__b_from_tbar_matched  = False
        self.__q1_from_t_matched    = False
        self.__q1_from_tbar_matched = False
        self.__q2_from_t_matched    = False
        self.__q2_from_tbar_matched = False

        # For sanity check
        self.__isLeptonic = False
        self.__isDilepton = False
        self.__isFullHadr = False
        self.__isUndef    = False

        self.__RecoJets  = {str(i)+"jet": RecoObject(self.__VariablesDict["pT_"+str(i)+"jet"],self.__VariablesDict["eta_"+str(i)+"jet"],self.__VariablesDict["phi_"+str(i)+"jet"],self.__VariablesDict["e_"+str(i)+"jet"]) if self.__VariablesDict["pT_"+str(i)+"jet"]!=0 else None for i in range(1,5)}
        self.__RecoLeptons  = {str(i)+"lep": RecoLepObject(self.__VariablesDict["pT_"+str(i)+"lep"],self.__VariablesDict["eta_"+str(i)+"lep"],self.__VariablesDict["phi_"+str(i)+"lep"],self.__VariablesDict["e_"+str(i)+"lep"],self.__VariablesDict["charge_"+str(i)+"lep"], self.__VariablesDict["type_"+str(i)+"lep"]) if self.__VariablesDict["pT_"+str(i)+"lep"]!=0 else None for i in range(1,5)}
        self.__TruthJets = {}
        self.__TruthLeptons = {}

        lepton_counter = 0
        ####################################################################
        ###################### b and Z reconstruction ######################
        ####################################################################
        self.__TruthLeptons["l1_from_Z"] = TruthLepObject(self.__VariablesDict["MC_Zdecay1_pt"],self.__VariablesDict["MC_Zdecay1_eta"],self.__VariablesDict["MC_Zdecay1_phi"],self.__VariablesDict["MC_Zdecay1_m"], self.__VariablesDict["MC_Zdecay1_pdgId"])
        self.__TruthLeptons["l2_from_Z"] = TruthLepObject(self.__VariablesDict["MC_Zdecay2_pt"],self.__VariablesDict["MC_Zdecay2_eta"],self.__VariablesDict["MC_Zdecay2_phi"],self.__VariablesDict["MC_Zdecay2_m"], self.__VariablesDict["MC_Zdecay2_pdgId"])
        self.__TruthJets["b_from_t"] = TruthObject(self.__VariablesDict["MC_b_from_t_pt"],self.__VariablesDict["MC_b_from_t_eta"],self.__VariablesDict["MC_b_from_t_phi"],self.__VariablesDict["MC_b_from_t_m"])
        self.__TruthJets["b_from_tbar"] = TruthObject(self.__VariablesDict["MC_b_from_tbar_pt"],self.__VariablesDict["MC_b_from_tbar_eta"],self.__VariablesDict["MC_b_from_tbar_phi"],self.__VariablesDict["MC_b_from_tbar_m"])

        ####################################################################
        ###################### t side reconstruction #######################
        ####################################################################
        if(abs(self.__VariablesDict["MC_Wdecay1_from_t_pdgId"]) in [1,2,3,4,5,6]):
            self.__TruthJets["q1_from_t"] = TruthObject(self.__VariablesDict["MC_Wdecay1_from_t_pt"],self.__VariablesDict["MC_Wdecay1_from_t_eta"],self.__VariablesDict["MC_Wdecay1_from_t_phi"],self.__VariablesDict["MC_Wdecay1_from_t_m"])
            self.__TruthJets["q2_from_t"] = TruthObject(self.__VariablesDict["MC_Wdecay2_from_t_pt"],self.__VariablesDict["MC_Wdecay2_from_t_eta"],self.__VariablesDict["MC_Wdecay2_from_t_phi"],self.__VariablesDict["MC_Wdecay2_from_t_m"])
            self.__TruthLeptons["l_from_t"] = None 
        elif (abs(self.__VariablesDict["MC_Wdecay1_from_t_pdgId"]) in [11,12,13,14,15,16]):
            self.__TruthJets["q1_from_t"] = None
            self.__TruthJets["q2_from_t"] = None
            if(abs(self.__VariablesDict["MC_Wdecay1_from_t_pdgId"]) in [11,13,15]):
                self.__TruthLeptons["l_from_t"] = TruthLepObject(self.__VariablesDict["MC_Wdecay1_from_t_pt"],self.__VariablesDict["MC_Wdecay1_from_t_eta"],self.__VariablesDict["MC_Wdecay1_from_t_phi"],self.__VariablesDict["MC_Wdecay1_from_t_m"], self.__VariablesDict["MC_Wdecay1_from_t_pdgId"])
                lepton_counter+=1
            elif(abs(self.__VariablesDict["MC_Wdecay2_from_t_pdgId"]) in [11,13,15]):
                self.__TruthLeptons["l_from_t"] = TruthLepObject(self.__VariablesDict["MC_Wdecay2_from_t_pt"],self.__VariablesDict["MC_Wdecay2_from_t_eta"],self.__VariablesDict["MC_Wdecay2_from_t_phi"],self.__VariablesDict["MC_Wdecay2_from_t_m"], self.__VariablesDict["MC_Wdecay2_from_t_pdgId"])
                lepton_counter+=1
        else:
            self.__isUndef = True
            self.__TruthJets["q1_from_t"] = None
            self.__TruthJets["q2_from_t"] = None
            self.__TruthLeptons["l_from_t"] = None
                
        ####################################################################
        #################### tbar side reconstruction ######################
        ####################################################################
        if(abs(self.__VariablesDict["MC_Wdecay1_from_tbar_pdgId"]) in [1,2,3,4,5,6]):
            self.__TruthJets["q1_from_tbar"] = TruthObject(self.__VariablesDict["MC_Wdecay1_from_tbar_pt"],self.__VariablesDict["MC_Wdecay1_from_tbar_eta"],self.__VariablesDict["MC_Wdecay1_from_tbar_phi"],self.__VariablesDict["MC_Wdecay1_from_tbar_m"])
            self.__TruthJets["q2_from_tbar"] = TruthObject(self.__VariablesDict["MC_Wdecay2_from_tbar_pt"],self.__VariablesDict["MC_Wdecay2_from_tbar_eta"],self.__VariablesDict["MC_Wdecay2_from_tbar_phi"],self.__VariablesDict["MC_Wdecay2_from_tbar_m"])
            self.__TruthLeptons["l_from_tbar"] = None 
        elif (abs(self.__VariablesDict["MC_Wdecay1_from_tbar_pdgId"]) in [11,12,13,14,15,16]):
            self.__TruthJets["q1_from_tbar"] = None
            self.__TruthJets["q2_from_tbar"] = None
            if(abs(self.__VariablesDict["MC_Wdecay1_from_tbar_pdgId"]) in [11,13,15]):
                self.__TruthLeptons["l_from_tbar"] = TruthLepObject(self.__VariablesDict["MC_Wdecay1_from_tbar_pt"],self.__VariablesDict["MC_Wdecay1_from_tbar_eta"],self.__VariablesDict["MC_Wdecay1_from_tbar_phi"],self.__VariablesDict["MC_Wdecay1_from_tbar_m"], self.__VariablesDict["MC_Wdecay1_from_tbar_pdgId"])
                lepton_counter+=1
            elif(abs(self.__VariablesDict["MC_Wdecay2_from_tbar_pdgId"]) in [11,13,15]):
                self.__TruthLeptons["l_from_tbar"] = TruthLepObject(self.__VariablesDict["MC_Wdecay2_from_tbar_pt"],self.__VariablesDict["MC_Wdecay2_from_tbar_eta"],self.__VariablesDict["MC_Wdecay2_from_tbar_phi"],self.__VariablesDict["MC_Wdecay2_from_tbar_m"], self.__VariablesDict["MC_Wdecay2_from_tbar_pdgId"])
                lepton_counter+=1
        else:
            self.__isUndef = True
            self.__TruthJets["q1_from_tbar"] = None
            self.__TruthJets["q2_from_tbar"] = None
            self.__TruthLeptons["l_from_tbar"] = None

        if(lepton_counter == 0):
            self.__isFullHadr = True
        if(lepton_counter == 1):
            self.__isLeptonic = True
        if(lepton_counter == 2):
            self.__isDilepton = True

        # Sanity check to make sure event is fullfilling exactly one category
        EventType = iter([self.__isFullHadr, self.__isLeptonic, self.__isDilepton])
        if not(any(EventType) and not any(EventType)) and not self.__isUndef:
            print("error")
            # Add error message here
            exit()

    def MatchWithTruth(self, nItems, option="Jets"):
        '''
        Method to match reco objects with truth objects.
        Keyword Arguments:
        nItems --- Number of items to be matched (e.g. 3 only consider first three objects)
        option --- Can be either "Jets" or "Leptons"
        '''
        if(option=="Jets"):
            # Truth jets
            b_from_t     = self.__TruthJets["b_from_t"]
            b_from_tbar  = self.__TruthJets["b_from_tbar"]
            q1_from_t    = self.__TruthJets["q1_from_t"]
            q2_from_t    = self.__TruthJets["q2_from_t"]
            q1_from_tbar = self.__TruthJets["q1_from_tbar"]
            q2_from_tbar = self.__TruthJets["q2_from_tbar"]
            
            RecoJetsVectors  = [item.Vector() for key,item in self.__RecoJets.items() if item !=None]
            TruthJetsVectors = [self.__TruthJets[key].Vector() for key,value in self.__JetIndeceDict.items() if self.__TruthJets[key]!=None]
            RecoJetIndece    = [-1]*len(RecoJetsVectors)
            TruthJetIndece   = [value for key,value in self.__JetIndeceDict.items() if self.__TruthJets[key]!=None]
            JetsToBeMatched  = [i for i in range(len(RecoJetsVectors))]

            MatchJetsRecoTruth(TruthJetIndece, TruthJetsVectors, RecoJetIndece, RecoJetsVectors, JetsToBeMatched, DR_cut = 0.3) # Here the matching happens for jets

            if(0 in RecoJetIndece):
                self.__b_from_t_matched = True
            if(1 in RecoJetIndece):
                self.__b_from_tbar_matched = True
            if(2 in RecoJetIndece):
                self.__q1_from_t_matched = True
            if(3 in RecoJetIndece):
                self.__q2_from_t_matched = True
            if(4 in RecoJetIndece):
                self.__q1_from_tbar_matched = True
            if(5 in RecoJetIndece):
                self.__q2_from_tbar_matched = True

            if(nItems > len(RecoJetIndece)):
               RecoJetIndece.extend([-2] * (nItems - len(RecoJetIndece)))
            return RecoJetIndece

        elif(option=="Leptons"):            
            # Truth leptons
            l1_from_Z   = self.__TruthLeptons["l1_from_Z"]
            l2_from_Z   = self.__TruthLeptons["l2_from_Z"]
            l_from_t    = self.__TruthLeptons["l_from_t"]
            l_from_tbar = self.__TruthLeptons["l_from_tbar"]

            RecoLeptonsVectors      = [item.Vector() for key,item in self.__RecoLeptons.items() if item !=None]
            TruthLeptonsVectors     = [self.__TruthLeptons[key].Vector() for key,value in self.__LeptonIndeceDict.items() if self.__TruthLeptons[key]!=None]
            RecoLeptonIndece        = [-1]*len(RecoLeptonsVectors)
            TruthLeptonIndece       = [value for key,value in self.__LeptonIndeceDict.items() if self.__TruthLeptons[key]!=None]
            LeptonsToBeMatched      = [i for i in range(len(RecoLeptonsVectors))]
            RecoLeptonsChargeType   = [item.ChargeType() for key, item in self.__RecoLeptons.items() if item != None]
            TruthLeptonsChargeType   = [item.ChargeType() for key, item in self.__TruthLeptons.items() if item != None]
 
            MatchLeptonsRecoTruth(TruthLeptonIndece, TruthLeptonsVectors, TruthLeptonsChargeType, RecoLeptonIndece, RecoLeptonsVectors, RecoLeptonsChargeType, LeptonsToBeMatched, DR_cut = 0.1) # Here the matching for leptons happens
            
            if(0 in RecoLeptonIndece and 1 in RecoLeptonIndece):
                self.__Z_matched = True
            elif((0 in RecoLeptonIndece) != (1 in RecoLeptonIndece)):
                self.__Z_half_matched = True

            if(2 in RecoLeptonIndece):
                self.__l_from_t_matched = True
            if(3 in RecoLeptonIndece):
                self.__l_from_tbar_matched = True
            
            if(nItems > len(RecoLeptonIndece)):
               RecoLeptonIndece.extend([-2] * (nItems - len(RecoLeptonIndece)))
            return RecoLeptonIndece
        else:
            ErrorMessage("Unrecognized reconstruction option for truth-reco matching!")

    def is3L(self):
        return self.__isLeptonic

    def is4L(self):
        return self.__isDilepton

    def isUndef(self):
        return self.__isUndef

    def isZMatched(self):
        return self.__Z_matched

    def isZHalfMatched(self):
        return self.__Z_half_matched

    def nNonZLeptonsMatched(self):
        return sum(self.__l_from_t_matched, self.__l_from_tbar_matched)

    def nZLeptonsMatched(self):
        if(isZMatched(self)):
            return 2
        elif(isZHalfMatched(self)):
            return 1
        else:
            return 0
        
    def nbMatched(self):
        return sum(self.__b_from_t_matched, self.__b_from_tbar_matched)

    def isHadronicMatched(self):
        return (self.is3L() and ((self.__q1_from_t_matched and self.__q2_from_t_matched and self.__b_from_t_matched) or (self.__q1_from_tbar_matched and self.__q2_from_tbar_matched and self.__b_from_tbar_matched)))

    def isLeptonicMatched(self):
        return (self.is3L() and ((self.__l_from_t_matched and self.__b_from_t_matched) or (self.__l_from_tbar_matched and self.__b_from_tbar_matched)))

    def isFullyMatched(self):
        return self.isHadronicMatched() and self.isLeptonicMatched()
                
    def get_ZIndece(self, Nlep):
        indece = self.MatchWithTruth(Nlep, option="Leptons")
        return [indece.index(i) if i in indece else -1 for i in [0,1]]
        
    def get_ZIndece_1D(self, Nlep):
        Zindece = self.get_ZIndece(Nlep)
        # if there are 3 leptons, the following combinations are sufficient:
        if -1 in Zindece:
            return -2
        elif 0 in Zindece and 1 in Zindece:
            return 0
        elif 0 in Zindece and 2 in Zindece:
            return 1
        elif 1 in Zindece and 2 in Zindece:
            return 2
        #if there are 4 leptons, one needs additional combinations:
        elif 0 in Zindece and 3 in Zindece:
            return 3
        elif 1 in Zindece and 3 in Zindece:
            return 4
        elif 2 in Zindece and 3 in Zindece:
            return 5
            
