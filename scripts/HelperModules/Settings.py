from numpy import unique

from itertools import groupby
from HelperModules.MessageHandler import ErrorMessage, WarningMessage, InfoDelimiter
from HelperModules.DNNModel import DNNModel
from HelperModules.BDTModel import BDTModel
from HelperModules.WGANModel import WGANModel
from HelperModules.Sample import Sample
from HelperModules.SystTrees import SystTrees
from HelperModules.Histograms import Histograms
from HelperModules.HelperFunctions import clear_config, get_groups, check_groups, ReadOption

class Settings:
    """ This class serves as a storage class for all settings that are read from the config file.

    Instance variables:
    option   --- Option to determine which settings are read and evaluated. Possible are 'all', 'samples' or 'model'.
    FileName --- File name (string) of the config file.
    model    --- String describing the type of model that shall be used later in the code.
    samples  --- List of samples (list of strings) that are picked up by the code and passed to the converter.
    general  --- Additional general settings which are defined in the General_Settings class.
    """
    def __init__(self, FileName, option="all", is_Train=True):
        self.__option   = option.lower()
        self.__FileName = FileName
        groups          = get_groups(clear_config(FileName))
        check_groups(groups)
        self.__model   = None
        self.__samples = []
        self.__general = None
        
        #####################################################################################
        ################################ Reading Settings ###################################
        #####################################################################################
        # Iterating through groups of settings (i.e. GENERAL, DNNMODEL/WGANMODEL and SAMPLE)
        for group in groups:
            # Detection of the GENERAL block
            if group[0]=="GENERAL" and (self.__option == "all" or self.__option == "samples"):
                self.__general = General_Settings(group)
            # Detection of each SAMPLE block
            if group[0]=="SAMPLE" and (self.__option == "all" or self.__option == "samples"):
                self.__samples.append(Sample(group))
            # Detection of the DNNModel block
            if group[0]=="DNNMODEL" and (self.__option == "all" or self.__option == "model"):
                self.__model = DNNModel(group,len(self.__general.get_Variables()), self.__general.get_Variables(), is_Train)
            # Detection of the BDTModel block
            if group[0]=="BDTMODEL" and (self.__option == "all" or self.__option == "model"):
                self.__model = BDTModel(group,len(self.__general.get_Variables()), self.__general.get_Variables(), is_Train)
            # Detection of the WGANModel block
            if group[0]=="WGANMODEL" and (self.__option == "all" or self.__option == "model"):
                self.__model = WGANModel(group,len(self.__general.get_Variables()), self.__general.get_Variables(), is_Train)

        #####################################################################################
        ############################### Errors and Warnings #################################
        #####################################################################################
        if len(unique([Sample.get_TrainLabel() for Sample in self.__samples if Sample.get_Group()!=None]))!=len(unique([Sample.get_Group() for Sample in self.__samples if Sample.get_Group()!=None])):
            ErrorMessage("Detected Groups with non-identical training labels. You need to check this!")

    ##############################################################################
    ############################### Misc methods #################################
    ##############################################################################
    def get_Model(self):
        return self.__model

    def get_Samples(self):
        return self.__samples

    def get_NominalSamples(self):
        return [Sample for Sample in self.__samples if Sample.get_Type()!="Systematic"] # only return non-systematic samples

    def get_SystSamples(self):
        return [Sample for Sample in self.__samples if Sample.get_Type()=="Systematic"] # only return systematic samples

    def get_General(self):
        return self.__general

    def Print(self):
        if("all" == self.__option or "model" == self.__option):
            print(self.__model)
            InfoDelimiter()
        if("all" == self.__option or "samples" == self.__option):
            print(self.__general)
            InfoDelimiter()
        if("all" == self.__option or "samples" == self.__option):
            for sample in self.__samples:
                print(sample)
                print()
                InfoDelimiter()
                    
class General_Settings:
    """ This class serves as a storage class for general settings that are read from the config file.

    Instance variables:
    Job             --- Job name. This name will be used to create a corresponding directory
    Selection       --- Selection (string) which is applied to all samples when the converter is called.
    MCWeight        --- MC weight (string) which is applied to all samples when the converter is called.
    Variables       --- Variables (list of strings) that are extracted from the root files and converted using the converter.
    Treename        --- Name of the input tree from which the Variables are extracted.
    InputScaling    --- Type of scaling that is applied to the inputs. Possible options: 'None', 'MinMax', 'MinMax_Symmetric' and 'Standard'.
    WeightScaling   --- Boolean. If set to 'True' weights are scaled such that the sum of weights of individual samples is unity. Weights are then devided by the overall mean.
    TreatNegWeights --- Boolean. If set to 'True' negative weights are set to zero.
    Folds           --- Number (integer) of folds that are used during K-Fold validation and in training. 
    ALabel          --- ATLAS Label.
    CMLabel         --- Centre of Mass label.
    MyLabel         --- Additional label to be printed on plots.
    Blinding        --- Float number above which data bins are blinded.
    """
    def __init__(self,group):
        self.__Job             = None
        self.__Selection       = ""
        self.__MCWeight        = ""
        self.__Variables       = None
        self.__Treename        = None
        self.__InputScaling    = None
        self.__WeightScaling   = True
        self.__TreatNegWeights = True
        self.__Folds           = 1
        self.__ATLASLabel      = ""
        self.__CMLabel         = ""
        self.__CustomLabel     = ""
        self.__DoCtrlPlots     = True
        self.__Blinding        = 999999 # All Histogram values beyond this point are blinded (default shouldn't blind)
        Hists = Histograms()

        #####################################################################################
        ################################ Reading Settings ###################################
        #####################################################################################
        for item in group:
            if "Job" in item:
                self.__Job = ReadOption(item, "Job", istype="str", islist=False)
            if "Selection" in item:
                self.__Selection = ReadOption(item, "Selection", istype="str", islist=False)
            if "MCWeight" in item:
                self.__MCWeight = ReadOption(item, "MCWeight", istype="str", islist=False)
            if "Treename" in item:
                self.__Treename = ReadOption(item, "Treename", istype="str", islist=False)
            if "Variables" in item:
                self.__Variables = ReadOption(item, "Variables", istype="str", islist=True)
            if "InputScaling" in item:
                self.__InputScaling = ReadOption(item, "InputScaling", istype="str", islist=False)
            if "WeightScaling" in item:
                self.__WeightScaling = ReadOption(item, "WeightScaling", istype="bool", islist=False, msg="Could not convert 'WeightScaling' into boolean. Make sure to pass either True or False.")
            if "TreatNegWeights" in item:
                self.__TreatNegWeights = ReadOption(item, "TreatNegWeights", istype="bool", islist=False, msg="Could not convert 'TreatNegWeights' into boolean. Make sure to pass either True or False.")
            if "Folds" in item:
                self.__Folds = ReadOption(item, "Folds", istype="int", islist=False, msg="Could not convert 'Folds' into integer.")
            if "ATLASLabel" in item:
                self.__ATLASLabel = ReadOption(item, "ATLASLabel", istype="str", islist=False)
            if "CMLabel" in item:
                Label = ReadOption(item, "CMLabel", istype="str", islist=False) # This is a little special because we can not parse '#' from the config file
                if(Label!=""):
                    self.__CMLabel = "#sqrt{s}="+Label
            if "CustomLabel" in item:
                self.__CustomLabel = ReadOption(item, "CustomLabel", istype="str", islist=False)
            if "DoCtrlPlots" in item:
                self.__DoCtrlPlots = ReadOption(item, "DoCtrlPlots", istype="bool", islist=False)
            if "Blinding" in item:
                self.__Blinding = ReadOption(item, "Blinding", istype="float", islist=False)

        #####################################################################################
        ############################### Errors and Warnings #################################
        #####################################################################################
        AllSystTrees = SystTrees() # Object holding all allowed syst trees
        if self.__Job == None:
            ErrorMessage("Could not find 'Job' option in general settings.")
        if self.__Treename == None:
            ErrorMessage("Could not find 'Treename' option in general settings.")
        if self.__Treename != "nominal" and self.__Treename != "nominal_Loose" and AllSystTrees.check_validity(self.__Treename):
            ErrorMessage("Given 'Treename' "+self.__Treename+" does not exist.")
        if self.__Variables == None:
            ErrorMessage("No list of variables given. Please provide a comma-separated list of valid variables.")
        if len(self.__Variables) > len(unique(self.__Variables)):
            ErrorMessage("List of variables given containts duplicates!")
        invalid_variables = [var for var in self.__Variables if not Hists.is_valid(var)]
        if len(invalid_variables)!=0:
            ErrorMessage("The following variables are not present in HelperFunctions/Histograms.py:\n"+'\n'.join(map(str, invalid_variables)))

    ##############################################################################
    ############################### Misc methods #################################
    ##############################################################################
    def get_Job(self):
        return self.__Job

    def get_Selection(self):
        return self.__Selection

    def get_MCWeight(self):
        return self.__MCWeight

    def get_Variables(self):
        return self.__Variables

    def get_Treename(self):
        return self.__Treename

    def get_InputScaling(self):
        return self.__InputScaling

    def get_WeightScaling(self):
        return self.__WeightScaling

    def get_TreatNegWeights(self):
        return self.__TreatNegWeights

    def get_Folds(self):
        return self.__Folds

    def get_ATLASLabel(self):
        return self.__ATLASLabel

    def get_CMLabel(self):
        return self.__CMLabel

    def get_CustomLabel(self):
        return self.__CustomLabel

    def get_Blinding(self):
        return self.__Blinding

    def do_CtrlPlots(self):
        return self.__DoCtrlPlots

    def __str__(self):
        return ("GENERALINFO:\tSelection: "+self.__Selection
                +"\nGENERALINFO:\tMCWeight: "+self.__MCWeight
                +"\nGENERALINFO:\tTreename: "+self.__Treename
                +"\nGENERALINFO:\tVariables: "+"\n\t\t".join(self.__Variables)
                +"\nGENERALINFO:\tInputScaling: "+self.__InputScaling
                +"\nGENERALINFO:\tWeightScaling: "+str(self.__WeightScaling)
                +"\nGENERALINFO:\tTreatNegWeights: "+str(self.__TreatNegWeights)
                +"\nGENERALINFO:\tFolds: "+str(self.__Folds))
