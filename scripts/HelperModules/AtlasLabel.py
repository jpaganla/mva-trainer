from ROOT import TLatex, gPad

def ATLASLabel(x,y,text,color=1):
      l = TLatex()
      l.SetNDC()
      l.SetTextFont(72)
      l.SetTextColor(color)
      #l.SetTextSize(0.06)
      l.SetTextSize(0.04)

      delx = 0.115*550*gPad.GetWh()/(472*gPad.GetWw())
      
      #delx = 0.115*500*gPad.GetWh()/(472*gPad.GetWw())

      l.DrawLatex(x,y,"ATLAS")

      if (text):
          p = TLatex()
          p.SetNDC()
          #p.SetTextSize(0.06)
          p.SetTextSize(0.04)
          p.SetTextFont(42)
          p.SetTextColor(color)
          p.DrawLatex(x+delx,y,text)

def CustomLabel(x,y,text, color = 1):
      p = TLatex()
      p.SetNDC()
      #p.SetTextSize(0.06)
      p.SetTextSize(0.04)
      p.SetTextFont(42)
      p.SetTextColor(color)
      p.DrawLatex(x,y,text)

def DrawLabels(ALabel, CMLabel, MyLabel, Option="data", align="left"):
      """Function to draw labels into plots

      Keyword arguments:
      ALabel  --- ATLAS Label.
      CMLabel --- Centre of Mass label.
      MyLabel --- Additional label to be printed on the plot.
      Option  --- Option to determine whether 'Simulation' is added to the label. Can be 'data' or 'MC'.
      align   --- Can be 'left' or 'right'.
      """
      if(align=="left"):
            x_pos = 0.2
      if(align=="right"):
            x_pos = 0.6
      if(align=="right_2"):
            x_pos = 0.5
      if(ALabel.lower()!="none"):
            if(Option=="data"):
                  Label = ALabel.replace("Simulation","")
                  ATLASLabel(x_pos,0.85, Label)
            elif(Option=="MC" and "Simulation" not in ALabel):
                  Label = "Simulation "+ALabel
                  ATLASLabel(x_pos,0.85, Label)
            else:
                  ATLASLabel(x_pos,0.85, ALabel)
      if(CMLabel!=""):
            CustomLabel(x_pos,0.8, CMLabel)
      if(MyLabel!=""):
            CustomLabel(x_pos,0.75, MyLabel)
