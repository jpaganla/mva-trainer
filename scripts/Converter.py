#!/usr/bin/env python3

import numpy as np
import pandas as pd
import argparse, copy, glob, os, ROOT

from root_numpy import root2array
from HelperModules import Settings
from multiprocessing import Pool
from PlotterModules.CtrlPlotHandler import CtrlPlotHandler
from HelperModules import EventReco
from HelperModules.SystTrees import SystTrees
from HelperModules.Directories import Directories
from HelperModules.HelperFunctions import ensure_trailing_slash, create_output_dir, sanitize_root
from HelperModules.MessageHandler import ConverterMessage, ConverterDoneMessage, ErrorMessage, WelcomeMessage
from shutil import copyfile

def convert(args, Job, Selection, Variables, Treename, MCWeight, Model, Sample):
    NtupleLocation = os.environ["TTZ_NTUPLE_LOCATION"]+os.environ["TTZ_NTUPLE_VERSION"]+"/"
    Dirs = Directories(Job)
    ConverterMessage("Processing "+Sample.get_Name()+" ("+Sample.get_Type()+")")
    SampleSelection = Sample.get_Selection()
    # For vectorized branches, fill values have to be defined in the variables to avoid c-type conversion problems
    SanitizedVariables = sanitize_root(Variables)

    # Check whether the sample selection has its own selection and append it
    if SampleSelection != "":
        Selection += "&&"+"("+SampleSelection+")"
    #######################################################################
    ############################ Classification ###########################
    #######################################################################
    if Model.isClassification():
        if Sample.get_TreeName()!=None:
            Treename=Sample.get_TreeName()
        X = root2array([NtupleLocation+FileString for FileString in Sample.get_NtupleFiles()], Treename, SanitizedVariables, Selection)
            
        # By default, vector-elements not found in ntuples are filled with
        # np.nan, which results in an error
        # TODO: Add fill-value support as an advanced option
        X_hasInvalid = np.array([np.isnan(X[0][i]).any() for i in range(len(Variables))])
        if X_hasInvalid.any():
            varnames_invalid = np.array(Variables)[X_hasInvalid]
            ErrorMessage("The following vectorized variables are not present "
                         + "in all events:", exit=False)
            for var in varnames_invalid:
                ErrorMessage(" - {:s}".format(var), exit=False)
                ErrorMessage("Please adjust variables or event selection so that "
                             + "every input variable is present in every event!")
        
        Y = [Sample.get_TrainLabel()]*len(X)
        if(Sample.get_Type()=="Data"):
            W = [1]*len(X)
        else:
            W = root2array([NtupleLocation+FileString for FileString in Sample.get_NtupleFiles()], Treename, MCWeight, Selection)
        if (Sample.get_Group()!=None):
            N = [Sample.get_Group()]*len(X)
        else:
            N = [Sample.get_Name()]*len(X)
        if Sample.get_Type() == "Data":
            isNominal = [True]*len(X)
        if Sample.get_Type() == "Systematic":
            isNominal = [False]*len(X)
        else:
            isNominal = [True]*len(X)
        T = [Sample.get_Type()]*len(X)
        df_X_all = pd.DataFrame(data = np.array(X), columns = Variables)
        df_Y_all = pd.DataFrame(data = np.array(Y), columns = ["Label"])
        df_W_all = pd.DataFrame(data = np.array(W), columns = ["Weight"])
        df_N_all = pd.DataFrame(data = np.array(N), columns = ["Sample_Name"])
        df_T_all = pd.DataFrame(data = np.array(T), columns = ["Sample_Type"])
        df_isNominal_all = pd.DataFrame(data = np.array(isNominal), columns = ["isNominal"])

    #######################################################################
    ############################ Reconstruction ###########################
    #######################################################################
    if Model.isReconstruction():
        # Producing a list of files. Since event numbers are not unique between samples we need to iterate over all files
        df_X_list, df_Y_list, df_W_list, df_N_list, df_T_list = ([] for i in range(5))
        Filepaths = []
        for FileString in Sample.get_NtupleFiles():
            Filepaths.extend(glob.glob(NtupleLocation+FileString))

        for Filepath in Filepaths:
            X = root2array(Filepath, Treename, SanitizedVariables, Selection)

            # By default, vector-elements not found in ntuples are filled with
            # np.nan, which results in an error
            # TODO: Add fill-value support as an advanced option
            X_hasInvalid = np.array([np.isnan(X[0][i]).any() for i in range(len(Variables))])
            if X_hasInvalid.any():
                varnames_invalid = Variables[X_hasInvalid]
                ErrorMessage("The following vectorized variables are not "
                               + "present in all events:", exit=False)
                for var in varnames_invalid:
                    ErrorMessage(" - {:s}".format(var), exit=False)
                ErrorMessage("Please adjust variables or event selection so "
                             + "that every input variable is present in every "
                             + "event!")

            df_X = pd.DataFrame(data = np.array(X), columns = Variables)
            if(Sample.get_Type()=="Data"):
                Y = [-1]*len(X)
                W = [1]*len(X)
            X_RecoVariables     = root2array(Filepath, Treename, EventReco.RecoVariables, Selection)
            X_TruthVariables    = root2array(Filepath, "truth", EventReco.TruthVariables, "")
            X_df_RecoVariables  = pd.DataFrame(data = np.array(X_RecoVariables), columns = EventReco.RecoVariables)
            X_df_TruthVariables = pd.DataFrame(data = np.array(X_TruthVariables), columns = EventReco.TruthVariables)
            X_merged = pd.merge(X_df_TruthVariables, X_df_RecoVariables, how = "inner", on=["eventNumber"])
            df_X = pd.merge(X_df_TruthVariables, df_X, how = "inner", on=["eventNumber"])
            Y = []
            for index, EventKinematics in X_merged.iterrows():
                EventKinematicsDict = EventKinematics.to_dict()
                e = EventReco.Event(EventKinematicsDict, "3L")
                # Z-Reconstruction
                if Model.isZReconstruction():
                    if (e.is3L()): 
                        Nlep = 3
                    elif(e.is4L()):
                        Nlep = 4
                    Y.append(e.get_ZIndece_1D(Nlep))
                # Add more reconstruction methods here
            if(Sample.get_Type()=="Data"):
                W = [1]*len(X_merged)
            else:
                # We have to load eventNumber and MCWeight separate from one another or MCWeight's type is evaluated wrongly
                eventNumbers_tmp = root2array(Filepath, Treename, "eventNumber", Selection)
                W_tmp = root2array(Filepath, Treename, MCWeight, Selection)
                W_df= pd.DataFrame(data=np.array(list(zip(W_tmp,eventNumbers_tmp))), columns = ["Weight","eventNumber"])
                W = pd.merge(X_df_TruthVariables, W_df, how = "inner", on=["eventNumber"])["Weight"].values
            N = [Sample.get_Name()]*len(X_merged)
            T = [Sample.get_Type()]*len(X_merged)

            removeTruth = EventReco.TruthVariables[:-1]
            df_X = df_X.drop(removeTruth, axis = 1) # Getting rid of truth variables again (except evenNumber which is the last item)
            df_X_list.append(df_X)
            df_Y_list.append(pd.DataFrame(data = np.array(Y), columns = ["Label"]))
            df_W_list.append(pd.DataFrame(data = np.array(W), columns = ["Weight"]))
            df_N_list.append(pd.DataFrame(data = np.array(N), columns = ["Sample_Name"]))
            df_T_list.append(pd.DataFrame(data = np.array(T), columns = ["Sample_Type"]))
        df_X_all = pd.concat(df_X_list)
        df_Y_all = pd.concat(df_Y_list)
        df_W_all = pd.concat(df_W_list)
        df_N_all = pd.concat(df_N_list)
        df_T_all = pd.concat(df_T_list)

    #######################################################################
    ############################## Merging of #############################
    ##############################  Datasets  #############################
    #######################################################################

    # Adding all temporary lists together
    df_all = pd.concat([df_X_all, df_Y_all, df_W_all, df_N_all, df_T_all, df_isNominal_all], axis=1, sort=False)
    df_all.to_hdf(ensure_trailing_slash(Dirs.DataDir())+Sample.get_Name()+".h5", key="df", mode="w")
    ConverterDoneMessage("Processing "+Sample.get_Name()+" ("+Sample.get_Type()+") DONE!")

if __name__ == "__main__":
    ROOT.gErrorIgnoreLevel = ROOT.kWarning
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-c", "--configfile", help="Config file", required=True)
    parser.add_argument("-f", "--filter", dest="wildcard", help="additional string to filter input files", type=str, default='')
    parser.add_argument("--processes", help="the number of parallel processes to run", type=int, default=8)
    args = parser.parse_args()

    WelcomeMessage("Converter")
    if os.environ.get("TTZ_NTUPLE_LOCATION") is None:
        ErrorMessage("TTZ_NTUPLE_LOCATION undefined. Did you forget to source the setup script?")
    if not os.path.isdir(os.environ["TTZ_NTUPLE_LOCATION"]+os.environ["TTZ_NTUPLE_VERSION"]+"/"):
        ErrorMessage(os.environ["TTZ_NTUPLE_LOCATION"]+os.environ["TTZ_NTUPLE_VERSION"]+"/ does not seem to exist. You need to check this!")

    # Here we create the necessary settings objects and read information from the config file
    cfg_Settings = Settings.Settings(args.configfile, option="all")
    Dirs = Directories(cfg_Settings.get_General().get_Job())

    # Copy the config file to the job directory
    copyfile(args.configfile, ensure_trailing_slash(Dirs.ConfigDir())+os.path.basename(args.configfile).replace(".cfg","conversion_step.cfg"))

    # Let's start some parallel processes to convert multiple files in parallel. Default is 8.
    # p = Pool(args.processes, maxtasksperchild=1)

    Job       = cfg_Settings.get_General().get_Job()
    Selection = cfg_Settings.get_General().get_Selection()
    Variables = cfg_Settings.get_General().get_Variables()+["eventNumber"]
    Treename  = cfg_Settings.get_General().get_Treename()
    MCWeight  = cfg_Settings.get_General().get_MCWeight()
    Model     = cfg_Settings.get_Model()
    for Sample in cfg_Settings.get_Samples():
        convert(args, Job, Selection, Variables, Treename, MCWeight, Model, Sample)

    # Parallel processing currently not working...
    # if len(cfg_Settings.get_Samples())==1:
    #     convert(args, args, Job, Selection, Variables, Treename, MCWeight, Model, cfg_Settings.get_Samples()[0])
    # else:
    #     fnames = [p.apply_async(convert, (args, Job, Selection, Variables, Treename, MCWeight, Model, Sample,)) for Sample in cfg_Settings.get_Samples()]
    #     p.close()
    #     p.join()

    # Now we merge all hdf5 files into one file call merged.h5
    ConverterMessage("Merging datasets...")
    dfs = [pd.read_hdf(fname) for fname in [ensure_trailing_slash(Dirs.DataDir())+Sample.get_Name()+".h5" for Sample in cfg_Settings.get_Samples()]]
    merged = pd.concat(dfs)

    # Shuffle the merged dataframes in place and reset index. Use random_state for reproducibility
    seed = np.random.randint(1000, size=1)[0]
    merged = merged.sample(frac=1, random_state = seed).reset_index(drop=True)

    merged.to_hdf(ensure_trailing_slash(Dirs.DataDir())+"merged.h5", key="df", mode="w")
    ConverterDoneMessage("Merged hdf5 file (data) written to "+ensure_trailing_slash(Dirs.DataDir())+"merged.h5")

    if cfg_Settings.get_Model().isClassification():
        if(cfg_Settings.get_General().do_CtrlPlots()==True):
            CH = CtrlPlotHandler(cfg_Settings)
            CH.do_CtrlPlots()
